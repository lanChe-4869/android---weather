package com.example.myapplicationfinalwork.controller;

import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.lifecycle.ViewModelProvider;

import com.example.myapplicationfinalwork.databinding.WeatherTodayFragmentBinding;
import com.example.myapplicationfinalwork.model.MapViewModel;
import com.example.myapplicationfinalwork.model.WeatherTodayViewModel;

import org.json.JSONException;
import org.json.JSONObject;


public class MyHandler {
    private final static String TAG="MyHandler";
    public static Handler handler=new Handler(Looper.getMainLooper()){
        @Override
        public void handleMessage(@NonNull Message msg) {
            super.handleMessage(msg);
            if(msg.what==1){
                Log.i(TAG, "handleMessage: "+msg.obj.toString());

            }
        }
    };
}
