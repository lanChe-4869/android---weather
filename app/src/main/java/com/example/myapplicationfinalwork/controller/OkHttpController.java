package com.example.myapplicationfinalwork.controller;

import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.util.Log;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;

import com.example.myapplicationfinalwork.common.Weather;
import com.example.myapplicationfinalwork.ui.weather.WeatherTodayFragment;
import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class OkHttpController {
    private final String TAG="OkHttpController";
    private Handler handler;
    private OkHttpClient okHttpClient;
    private List<Weather> listWeather=new ArrayList<>();


    public OkHttpController(Handler handler){
        okHttpClient = new OkHttpClient.Builder().connectTimeout(10, TimeUnit.SECONDS).readTimeout(10,TimeUnit.SECONDS)
                .writeTimeout(10, TimeUnit.SECONDS)
                .build();
        this.handler=handler;
    }

    public void getWeather(String city,String date){

        String url="http://wthrcdn.etouch.cn/weather_mini?city="+city;
        Log.i(TAG, "getWeather: "+url);
        Request request=new Request.Builder().url(url).build();
        Call call=okHttpClient.newCall(request);

        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.i(TAG, "onFailure: "+e.getMessage());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String ans=response.body().string();
                try {
                    JSONObject obj=new JSONObject(ans);
                    if(!obj.getString("desc").equals("OK")){
                        return;
                    }
                    listWeather.clear();
                    JSONObject data=obj.getJSONObject("data");
                    String city=data.getString("city");

                    JSONArray forecast=data.getJSONArray("forecast");
                    Log.i(TAG, "onResponse: "+forecast);
                    Gson gson=new Gson();

                    Bundle bundle=new Bundle();
                    bundle.putString("nowTem",data.getString("wendu"));
                    bundle.putString("date",date);

                    Message msg=new Message();
                    msg.what=1;
                    msg.obj=data;
                    msg.setData(bundle);
                    handler.sendMessage(msg);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }

    public void getWeatherList(String city,int what){

        String url="http://wthrcdn.etouch.cn/weather_mini?city="+city;
        Log.i(TAG, "getWeather: "+url);
        Request request=new Request.Builder().url(url).build();
        Call call=okHttpClient.newCall(request);

        call.enqueue(new Callback() {
            @Override
            public void onFailure(Call call, IOException e) {
                Log.i(TAG, "onFailure: "+e.getMessage());
            }

            @Override
            public void onResponse(Call call, Response response) throws IOException {
                String ans=response.body().string();
                try {
                    JSONObject obj=new JSONObject(ans);
                    if(!obj.getString("desc").equals("OK")){
                        return;
                    }
                    JSONObject data=obj.getJSONObject("data");
                    String city=data.getString("city");

                    JSONArray forecast=data.getJSONArray("forecast");
                    Log.i(TAG, "onResponse: "+forecast);
                    Gson gson=new Gson();
                    Weather today=gson.fromJson((forecast.getJSONObject(0)).toString(),Weather.class);

                    Bundle bundle=new Bundle();
                    bundle.putString("nowTem",data.getString("wendu"));
                    bundle.putString("cityname",data.getString("city"));

                    Message msg=new Message();
                    msg.what=what;
                    msg.obj=today;
                    msg.setData(bundle);
                    handler.sendMessage(msg);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });
    }
}
