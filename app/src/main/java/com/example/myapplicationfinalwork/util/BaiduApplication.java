package com.example.myapplicationfinalwork.util;

import android.app.Application;
import android.content.Context;

import com.baidu.mapapi.CoordType;
import com.baidu.mapapi.SDKInitializer;

public class BaiduApplication extends Application {
    public static Context context;
    public String username;
    public String localedCity;

    @Override
    public void onCreate() {
        super.onCreate();
        context=this;

        SDKInitializer.initialize(this);

        //包括BD09LL和GCJ02两种坐标，默认是BD09LL坐标。
        SDKInitializer.setCoordType(CoordType.BD09LL);

        // 全局变量
        username="兰澈";
    }
}
