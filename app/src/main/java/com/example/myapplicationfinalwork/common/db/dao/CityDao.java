package com.example.myapplicationfinalwork.common.db.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapplicationfinalwork.common.db.table.InterestedCity;

import java.util.List;

@Dao
public interface CityDao {
    @Insert
    long addCity(InterestedCity city);

    @Update
    void updateCity(InterestedCity city);

    @Delete
    void deleteCity(InterestedCity city);

    @Query("SELECT cityName FROM city where username=:username order by time desc limit :num")
    List<String> getCityName(int num,String username);

    @Query("select * from city where cityName=:cityName and username=:username")
    List<InterestedCity> getSelectedCity(String cityName,String username);

    @Query("SELECT cityName FROM city where username=:username order by time desc")
    List<String> getAllCityName(String username);

    @Query("select * from city")
    List<InterestedCity> test();

    @Query("select * from city where username=:username order by time desc limit :num")
    List<InterestedCity> getAllCity(String username,int num);
}
