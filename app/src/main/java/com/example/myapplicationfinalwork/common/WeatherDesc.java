package com.example.myapplicationfinalwork.common;

public class WeatherDesc {
    public String type;
    public String city, tem;

    public WeatherDesc(String city, String tem, String type) {
        this.city = city;
        this.tem = tem;
        this.type = type;
    }
}
