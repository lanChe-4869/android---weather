package com.example.myapplicationfinalwork.common.db.dao;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

import com.example.myapplicationfinalwork.common.db.table.UserTable;

@Dao
public interface UserDao {
    @Insert
    long addUser(UserTable user);

    @Update
    void updateUser(UserTable user);

    @Delete
    void deleteUser(UserTable user);

    @Query("select * from user where username=:username")
    UserTable getUser(String username);

    @Query("select count(*) from user where username=:username and pwd=:pwd")
    int loadUser(String username,String pwd);
}
