package com.example.myapplicationfinalwork.common;

public class HistoryWeather {
    private String city;
    private String nowTem;
    private int[] high;
    private int[] low;
    private String date;

    public HistoryWeather(String city, String nowTem, int[] high, int[] low,String date) {
        this.city = city;
        this.nowTem = nowTem;
        this.high = high;
        this.low = low;
        this.date=date;
    }

    public HistoryWeather() {
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getNowTem() {
        return nowTem;
    }

    public void setNowTem(String nowTem) {
        this.nowTem = nowTem;
    }

    public int[] getHigh() {
        return high;
    }

    public void setHigh(int[] high) {
        this.high = high;
    }

    public int[] getLow() {
        return low;
    }

    public void setLow(int[] low) {
        this.low = low;
    }

    public void setDate(String date){
        this.date=date;
    }

    public String getDate(){
        return this.date;
    }
}
