package com.example.myapplicationfinalwork.common;

import android.text.InputType;
import android.text.method.HideReturnsTransformationMethod;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.widget.EditText;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.TimeZone;

public class Analysis {
    private final static String TAG="myDebug";

    public static String calFengli(String str){
        int st=0;
        boolean flag=true;
        while(st<str.length()){
            char ch=str.charAt(st);
            ++st;
            if(ch=='['){
                if(flag){
                    flag=false;
                }
                else break;
            }
        }
        int ed=st+1;
        while(ed<str.length()){
            char ch=str.charAt(ed);
            if(ch==']')
                break;
            ++ed;
        }
        return str.substring(st,ed);
    }

    public static String calTemperature(String str){
        int st=0;
        while(st<str.length()){
            char ch=str.charAt(st);
            if(ch>='0'&&ch<='9')
                break;
            ++st;
        }
        return str.substring(st);
    }

    public static int getNumOfTemperature(String str){
        str=calTemperature(str);
        int st=0;
        while(st<str.length()){
            char ch=str.charAt(st);
            if(ch>='0'&&ch<='9') ++st;
            else break;
        }
        return Integer.valueOf(str.substring(0,st));
    }

    public static void EditTextEnable(boolean enable, EditText editText){
        editText.setFocusable(enable);
        editText.setFocusableInTouchMode(enable);
        editText.setLongClickable(enable);
        editText.setInputType(enable? InputType.TYPE_CLASS_TEXT:InputType.TYPE_TEXT_VARIATION_PASSWORD);
        editText.setTransformationMethod(enable? HideReturnsTransformationMethod.getInstance(): PasswordTransformationMethod.getInstance());
    }

    public static String getDateFormat(){
        SimpleDateFormat formatter = new SimpleDateFormat("YYYY-MM-dd HH:mm");
        formatter.setTimeZone(TimeZone.getTimeZone("GMT+08"));
        Date curDate = new Date(System.currentTimeMillis());
        String strDate = formatter.format(curDate);
        Log.i(TAG, "calDateFormat: "+strDate);
        return strDate;
    }
}
