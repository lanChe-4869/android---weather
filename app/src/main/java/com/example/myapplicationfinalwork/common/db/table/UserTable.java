package com.example.myapplicationfinalwork.common.db.table;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "user")
public class UserTable {
    @PrimaryKey(autoGenerate = false)
    @ColumnInfo(name="username")
    private @NonNull String username;

    @ColumnInfo(name="pwd")
    private String pwd;

    @ColumnInfo(name="display_num")
    private int displayNum;

    @ColumnInfo(name="auto_login")
    private boolean autoLogin;

    // 当前城市存在application的全局变量当中


    public UserTable(String username, String pwd) {
        this.username = username;
        this.pwd = pwd;
        this.displayNum=4;
        this.autoLogin=false;
    }

    public boolean isAutoLogin() {
        return autoLogin;
    }

    public void setAutoLogin(boolean autoLogin) {
        this.autoLogin = autoLogin;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public int getDisplayNum() {
        return displayNum;
    }

    public void setDisplayNum(int displayNum) {
        this.displayNum = displayNum;
    }
}
