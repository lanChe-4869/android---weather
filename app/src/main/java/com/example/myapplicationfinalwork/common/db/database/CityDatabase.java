package com.example.myapplicationfinalwork.common.db.database;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.migration.Migration;
import androidx.sqlite.db.SupportSQLiteDatabase;

import com.example.myapplicationfinalwork.common.db.dao.CityDao;
import com.example.myapplicationfinalwork.common.db.table.InterestedCity;
import com.example.myapplicationfinalwork.util.BaiduApplication;

@Database(entities = {InterestedCity.class},version = 2,exportSchema = false)
public abstract class CityDatabase extends RoomDatabase {
    private static CityDatabase INSTANCE;

    static final Migration MIGRATION_1_2 = new Migration(1,2) {
        @Override
        public void migrate(@NonNull SupportSQLiteDatabase database) {
            database.execSQL("alter table city add username varchar(50)");
        }
    };


    public static CityDatabase getInstance() {
        if (INSTANCE == null) {
            synchronized (CityDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(BaiduApplication.context, CityDatabase.class, "city.db")
                            .addMigrations(MIGRATION_1_2)
                            .allowMainThreadQueries()
                            .build();
                }
            }
        }
        return INSTANCE;
    }



    public abstract CityDao getCityDao();
}
