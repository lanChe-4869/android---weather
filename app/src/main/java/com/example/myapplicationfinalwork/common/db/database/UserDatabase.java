package com.example.myapplicationfinalwork.common.db.database;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import com.example.myapplicationfinalwork.common.db.dao.CityDao;
import com.example.myapplicationfinalwork.common.db.dao.UserDao;
import com.example.myapplicationfinalwork.common.db.table.InterestedCity;
import com.example.myapplicationfinalwork.common.db.table.UserTable;
import com.example.myapplicationfinalwork.util.BaiduApplication;

@Database(entities = {UserTable.class},version = 1,exportSchema = false)
public abstract class UserDatabase extends RoomDatabase {
    private static UserDatabase INSTANCE;


    public static UserDatabase getInstance() {
        if (INSTANCE == null) {
            synchronized (UserDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(BaiduApplication.context, UserDatabase.class, "user.db")
                            .allowMainThreadQueries()
                            .build();
                }
            }
        }
        return INSTANCE;
    }

    public abstract UserDao getUserDao();
}
