package com.example.myapplicationfinalwork.model;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.myapplicationfinalwork.common.Weather;
import com.example.myapplicationfinalwork.common.db.database.CityDatabase;
import com.example.myapplicationfinalwork.common.db.database.UserDatabase;
import com.example.myapplicationfinalwork.common.db.table.UserTable;

import java.util.List;

public class WeatherTodayViewModel extends ViewModel {
    // TODO: Implement the ViewModel
    private MutableLiveData<String> nowTem=new MutableLiveData<>("11℃");
    private MutableLiveData<String> tem=new MutableLiveData<>("12℃/-3℃");
    private MutableLiveData<String> wind=new MutableLiveData<String>("北风  1级");
    private MutableLiveData<String> type=new MutableLiveData<String>("晴");
    private MutableLiveData<List<Weather>> listForecast=new MutableLiveData<List<Weather>>();
    private MutableLiveData<List<String>> listCity;

    public void setNowTem(String val){
        nowTem.setValue(val);
    }

    public MutableLiveData<String> getNowTem(){
        if(nowTem==null)
            nowTem=new MutableLiveData<>("11℃");
        return nowTem;
    }

    public void setTem(String val){
        tem.setValue(val);
    }

    public MutableLiveData<String> getTem(){
        return tem;
    }

    public void setWind(String val){
        wind.setValue(val);
    }

    public MutableLiveData<String> getWind(){
        return wind;
    }

    public void setType(String val){
        type.setValue(val);
    }

    public MutableLiveData<String> getType(){
        return type;
    }

    public void setListForecast(List<Weather> list){
        listForecast.setValue(list);
    }

    public MutableLiveData<List<Weather>> getListForecast(){
        return listForecast;
    }

    public MutableLiveData<List<String>> getListCity(String username){
        if(listCity==null){
            listCity=new MutableLiveData<>();
            setListCity(username);
        }
        return listCity;
    }

    public void setListCity(String username){
        UserTable user= UserDatabase.getInstance().getUserDao().getUser(username);
        int displayNum=user.getDisplayNum();
        List<String> ans= CityDatabase.getInstance().getCityDao().getCityName(displayNum,username);
        this.listCity.setValue(ans);
    }
}