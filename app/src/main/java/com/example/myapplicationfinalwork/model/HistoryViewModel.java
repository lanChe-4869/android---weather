package com.example.myapplicationfinalwork.model;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.myapplicationfinalwork.MainActivity;
import com.example.myapplicationfinalwork.common.HistoryWeather;
import com.example.myapplicationfinalwork.common.db.database.CityDatabase;
import com.example.myapplicationfinalwork.common.db.database.UserDatabase;
import com.example.myapplicationfinalwork.common.db.table.InterestedCity;
import com.example.myapplicationfinalwork.common.db.table.UserTable;

import java.util.ArrayList;
import java.util.List;

public class HistoryViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    private MutableLiveData<List<HistoryWeather>> listData=new MutableLiveData<>();

    private MutableLiveData<List<InterestedCity>> listCity;

    public HistoryViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is history fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }

    public void setListData(List<HistoryWeather> mListData) {
        this.listData.setValue(mListData);
    }

    public MutableLiveData<List<HistoryWeather>> getListData(){
        return listData;
    }

    public MutableLiveData<List<InterestedCity>> getListCity(String username){
        if(listCity==null){
            listCity=new MutableLiveData<>();

            int num=UserDatabase.getInstance().getUserDao().getUser("兰澈").getDisplayNum();

            List<InterestedCity> tmp=CityDatabase.getInstance().getCityDao().getAllCity(username,num);
            listCity.setValue(tmp);
            /*List<String> tmp=new ArrayList<>();
            tmp.add("北京市");
            tmp.add("杭州市");
            listCity.setValue(tmp);*/
        }
        return listCity;
    }

    public void setListCity(List<InterestedCity> mListCity){
        this.listCity.setValue(mListCity);
    }

}