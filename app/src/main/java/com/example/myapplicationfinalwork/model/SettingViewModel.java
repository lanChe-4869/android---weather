package com.example.myapplicationfinalwork.model;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.myapplicationfinalwork.MainActivity;
import com.example.myapplicationfinalwork.common.db.database.CityDatabase;

import java.util.List;

public class SettingViewModel extends ViewModel {
    // TODO: Implement the ViewModel
    private MutableLiveData<String> mText;
    private MutableLiveData<List<String>> cityName;

    public SettingViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is setting fragment");
    }

    public LiveData<String> getText() {
        return mText;
    }

    public MutableLiveData<List<String>> getCityName(String username){
        if(cityName==null){
            List<String> tmp = CityDatabase.getInstance().getCityDao().getAllCityName(username);
            cityName=new MutableLiveData<>();
            cityName.setValue(tmp);
        }
        return cityName;
    }

    public void setCityName(String username){
        List<String> tmp = CityDatabase.getInstance().getCityDao().getAllCityName(username);
        cityName.setValue(tmp);
    }

}