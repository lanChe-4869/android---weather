package com.example.myapplicationfinalwork.model;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class MapViewModel extends ViewModel {

    private MutableLiveData<String> mText;
    private MutableLiveData<String> mCity;

    public MapViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("This is map fragment");
        mCity=new MutableLiveData<>("上海");
    }

    public LiveData<String> getText() {
        return mText;
    }

    public void setCity(String city){
        mCity.setValue(city);
    }

    public MutableLiveData<String> getCity(String city){
        if(mCity==null)
            mCity=new MutableLiveData<String>(city);
        return mCity;
    }
}