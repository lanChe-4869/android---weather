package com.example.myapplicationfinalwork;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;

import com.example.myapplicationfinalwork.common.db.database.UserDatabase;
import com.example.myapplicationfinalwork.common.db.table.UserTable;
import com.example.myapplicationfinalwork.ui.weather.WeatherRecommendFragment;
import com.example.myapplicationfinalwork.ui.weather.WeatherTodayFragment;
import com.google.android.material.bottomnavigation.BottomNavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.AppBarConfiguration;
import androidx.navigation.ui.NavigationUI;

import com.example.myapplicationfinalwork.databinding.ActivityMainBinding;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private ActivityMainBinding binding;
    public String username;
    public String localedCity;
    public boolean flag=true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityMainBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        BottomNavigationView navView = findViewById(R.id.nav_view);
        // Passing each menu ID as a set of Ids because each
        // menu should be considered as top level destinations.
        AppBarConfiguration appBarConfiguration = new AppBarConfiguration.Builder(
                R.id.navigation_map, R.id.navigation_weather, R.id.navigation_history,R.id.navigation_setting)
                .build();
        NavController navController = Navigation.findNavController(this, R.id.nav_host_fragment_activity_main);
        NavigationUI.setupActionBarWithNavController(this, navController, appBarConfiguration);
        NavigationUI.setupWithNavController(binding.navView, navController);

        // 设置全局变量
        Intent intent=getIntent();
        username=intent.getStringExtra("username");

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.overflow_menu,menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onMenuOpened(int featureId, Menu menu) {
        return super.onMenuOpened(featureId, menu);
    }

    public FragmentManager viewGetFragmentManager(){
        return getSupportFragmentManager();
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        SharedPreferences sharedPreferences = getSharedPreferences("user",MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        switch (item.getItemId()){
            case R.id.overflowMenu_LoginOut:
                UserTable user1=UserDatabase.getInstance().getUserDao().getUser(this.username);
                user1.setAutoLogin(false);
                UserDatabase.getInstance().getUserDao().updateUser(user1);
                editor.putBoolean("isAutoLogin",false);
                editor.commit();
                Intent intent=new Intent(MainActivity.this, LoginActivity.class); startActivity(intent);
                return true;
            case R.id.overflowMenu_deleteUser:
                editor.clear();
                editor.commit();
                UserTable user2=UserDatabase.getInstance().getUserDao().getUser(this.username);
                UserDatabase.getInstance().getUserDao().deleteUser(user2);
                Intent intent1=new Intent(MainActivity.this, LoginActivity.class); startActivity(intent1);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}