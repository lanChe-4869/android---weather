package com.example.myapplicationfinalwork;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.example.myapplicationfinalwork.common.db.database.CityDatabase;
import com.example.myapplicationfinalwork.common.db.database.UserDatabase;
import com.example.myapplicationfinalwork.common.db.table.InterestedCity;
import com.example.myapplicationfinalwork.common.db.table.UserTable;
import com.example.myapplicationfinalwork.databinding.ActivityLoginBinding;
import com.example.myapplicationfinalwork.databinding.ActivityMainBinding;

import java.util.List;

public class LoginActivity extends AppCompatActivity {
    private ActivityLoginBinding binding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        binding = ActivityLoginBinding.inflate(getLayoutInflater());
        setContentView(binding.getRoot());

        SharedPreferences sharedPreferences = getSharedPreferences("user",MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        String iUsername=sharedPreferences.getString("username","");
        binding.loginUsername.setText(iUsername);
        boolean isAutoLogin=sharedPreferences.getBoolean("isAutoLogin",false);
        if(isAutoLogin){
            toMainActivity(iUsername);
            return;
        }

        binding.loginLoadBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String username=binding.loginUsername.getText().toString();
                String pwd=binding.loginPwd.getText().toString();
                UserTable user=UserDatabase.getInstance().getUserDao().getUser(username);
                if(user==null){
                    Toast.makeText(LoginActivity.this, "不存在用户名: "+username, Toast.LENGTH_SHORT).show();
                }
                else if(!user.getPwd().equals(pwd)){
                    Toast.makeText(LoginActivity.this, "密码错误", Toast.LENGTH_SHORT).show();
                }
                else{
                    editor.putString("username",username);
                    editor.commit();
                    toMainActivity(username);

                }
            }
        });

        binding.loginRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });

        binding.loginRegister.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                View view=View.inflate(LoginActivity.this, R.layout.alert_register, null);

                EditText username=view.findViewById(R.id.register_username),
                        pwd=view.findViewById(R.id.register_pwd),
                        pwd_confirm=view.findViewById(R.id.register_pwd_confirm);

                AlertDialog.Builder builder=new AlertDialog.Builder(LoginActivity.this);
                builder.setTitle("注册用户");
                builder.setView(view);

                //设置确定按钮
                builder.setPositiveButton("注册", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(!pwd.getText().toString().equals(pwd_confirm.getText().toString())){
                            Toast.makeText(LoginActivity.this,"确认密码错误",Toast.LENGTH_SHORT).show();
                        }
                        else{
                            UserTable user=new UserTable(username.getText().toString(),pwd.getText().toString());
                            UserDatabase.getInstance().getUserDao().addUser(user);
                            Toast.makeText(LoginActivity.this,"用户: "+user.getUsername()+" 创建成功",Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                //设置取消按钮
                builder.setNegativeButton("返回",null);
                //显示提示框
                builder.show();
            }
        });
    }

    void toMainActivity(String username){
        Intent intent=new Intent(LoginActivity.this, MainActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK|Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.putExtra("username",username);
        startActivity(intent);
    }
}