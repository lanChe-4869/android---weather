package com.example.myapplicationfinalwork.ui.weather;

import androidx.lifecycle.ViewModelProvider;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.os.Bundle;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.provider.MediaStore;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.myapplicationfinalwork.R;
import com.example.myapplicationfinalwork.databinding.WeatherRecommendFragmentBinding;
import com.example.myapplicationfinalwork.databinding.WeatherTodayFragmentBinding;
import com.example.myapplicationfinalwork.model.WeatherRecommendViewModel;
import com.example.myapplicationfinalwork.util.QRCode;

public class WeatherRecommendFragment extends Fragment {

    private WeatherRecommendViewModel mViewModel;
    private WeatherRecommendFragmentBinding binding;

    public static WeatherRecommendFragment newInstance() {
        return new WeatherRecommendFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        binding= WeatherRecommendFragmentBinding.inflate(inflater,container,false);
        mViewModel = new ViewModelProvider(this).get(WeatherRecommendViewModel.class);

        binding.QRGenerate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String content=binding.QRText.getText().toString();
                binding.QRCode.setImageBitmap(QRCode.createQRCodeBitmap(content, 500, 500,"UTF-8","H", "1", Color.BLACK, Color.WHITE));
            }
        });

        binding.btnDayPic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                Log.i("myDebug", "onClick: 开启相机");
                startActivityForResult(intent, 2);
                //Log.i("myDebug", "onClick: 开启相机");
            }
        });

        return binding.getRoot();
    }

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        // TODO: Use the ViewModel
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 2 && null != data) {
            Bundle bundle = data.getExtras();
            //intent跳转携带参数返回，data转化成Bitmap，获得的是个略缩图
            Bitmap photo = (Bitmap) bundle.get("data");
            //将预览图放进预览框
            binding.imgDayPic.setImageBitmap(photo);
        }
    }
}