package com.example.myapplicationfinalwork.ui.weather;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.viewpager.widget.ViewPager;


import com.example.myapplicationfinalwork.MainActivity;
import com.example.myapplicationfinalwork.databinding.FragmentWeatherBinding;
import com.example.myapplicationfinalwork.model.WeatherViewModel;
import com.google.android.material.tabs.TabLayout;

import java.util.ArrayList;
import java.util.List;
import com.example.myapplicationfinalwork.R;

public class WeatherFragment extends Fragment {
    final String TAG="weatherFragment";

    private WeatherViewModel weatherViewModel;
    private FragmentWeatherBinding binding;
    private TabLayout mtabs;
    private ViewPager viewPager;
    private List<Fragment> fragments;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        
        weatherViewModel =
                new ViewModelProvider(this).get(WeatherViewModel.class);

        binding = FragmentWeatherBinding.inflate(inflater, container, false);
        View root = binding.getRoot();

        final TextView textView = binding.textDashboard;
        weatherViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        initViewPager();
        return root;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    private void initViewPager(){
        mtabs=binding.weatherTabs;

        viewPager= binding.viewPager;
        fragments=new ArrayList<>();
        WeatherTodayFragment model1=new WeatherTodayFragment();
        WeatherRecommendFragment model2=new WeatherRecommendFragment();
        fragments.add(model1);
        fragments.add(model2);

        List<String>listStr=new ArrayList<>();
        listStr.add(getResources().getString(R.string.tab_text_1));
        listStr.add(getResources().getString(R.string.tab_text_2));

        //((MainActivity)getActivity()).viewGetFragmentManager()
        SectionsPagerAdapter sectionsPagerAdapter=new SectionsPagerAdapter(getChildFragmentManager(),fragments,listStr);
        viewPager.setAdapter(sectionsPagerAdapter);
        mtabs.setupWithViewPager(viewPager);
        Log.i(TAG, "onCreateView: "+fragments.size());
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        Log.i(TAG, "onDestroyView: ");
        binding = null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.i(TAG, "onDestroy: ");
    }

    private void showFragment(int position) {

        // viewPage中的适配器，按照索引进行显示
        viewPager.setCurrentItem(position);  // 设置当前显示的位置
    }
}