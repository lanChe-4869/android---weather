package com.example.myapplicationfinalwork.ui.weather;

import android.content.Context;
import android.util.Log;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.annotation.StringRes;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentPagerAdapter;
import androidx.fragment.app.FragmentTransaction;

import com.example.myapplicationfinalwork.R;

import java.util.List;

/**
 * A [FragmentPagerAdapter] that returns a fragment corresponding to
 * one of the sections/tabs/pages.
 */
public class SectionsPagerAdapter extends FragmentPagerAdapter {
    final String TAG="weatherFragment";
    private List<String> tabNames;

    // 定义Fragment列表来存放Fragment
    private List<Fragment> fragmentList;
    // 1. 定义构造方法
    public SectionsPagerAdapter(@NonNull FragmentManager fm, List<Fragment> listFragment,List<String> listTabs) {
        super(fm);
        this.fragmentList = listFragment;
        this.tabNames=listTabs;
        //Log.i(TAG, "SectionsPagerAdapter: debug");
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return tabNames.get(position);
    }


    @NonNull
    @Override
    // 2. 显示页面，为数组中的Fragment’
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    // 3. 获取页面的个数，几位列表的长度
    public int getCount() {
        return fragmentList.size();
    }
}